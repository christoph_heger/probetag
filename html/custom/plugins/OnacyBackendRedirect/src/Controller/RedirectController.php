<?php declare(strict_types=1);

namespace Onacy\BackendRedirect\Controller;

use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
/**
 * @RouteScope(scopes={"storefront"})
 */
class RedirectController extends StorefrontController
{
    /**
     * @Route("/backend", name="onacy.backend", methods={"GET"})
     */
    public function backendRedirect(Request $request, RequestDataBag $data, SalesChannelContext $context): Response
    {
    	$redirect = $request->get('redirectTo', 'administration.index');
        return $this->redirectToRoute($redirect);
    }
}