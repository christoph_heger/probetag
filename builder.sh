#!/usr/bin/env bash
VM_HOSTNAME=probetag
VM_SHOPWARE=/var/www/shopware

BUTLER=/srv/www/BUTLER
DATE_WITH_TIME=`date "+%Y-%m-%d-%H:%M:%S"`

if [ ! -e $BUTLER ]; then

sudo DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::='--force-confold' --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages -uy update
sudo DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::='--force-confold' --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages -fuy dist-upgrade

## ENV PACKAGES ##

sudo apt install -yf apache2 php-apcu curl imagemagick unzip git composer mysql-server build-essential git apt-transport-https uuid-runtime pwgen gnupg redis-server

## ENV PACKAGES ##

## PHP 7.3 and extensions

sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt -y update

sudo apt install -yf php7.3 php7.3-fpm php7.3-curl php7.3-gd php7.3-dom php7.3-imagick php7.3-opcache php7.3-zip php7.3-simplexml php7.3-xml php7.3-iconv php7.3-intl php7.3-json php7.3-mbstring libapache2-mod-php7.3 php7.3-mysql php7.3-redis php7.3-gmp

## PHP 7.3 and extensions

## MYSQL ##

## NODE JS ##

curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt install -y nodejs

## NODE JS ##

## ELASTICSEARCH 7.x ##
## https://computingforgeeks.com/install-elasticsearch-on-ubuntu/ ##
## https://computingforgeeks.com/easy-way-to-delete-elasticsearch-index-data/ ##

sudo wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" | sudo tee  /etc/apt/sources.list.d/elastic-7.x.list
sudo apt update
sudo apt install elasticsearch-oss
sudo echo "cluster.name: es-cluster" >> /etc/elasticsearch/elasticsearch.yml
sudo systemctl enable elasticsearch.service && sudo systemctl restart elasticsearch.service

## ELASTICSEARCH ##

## TIMEZONE ##

sudo timedatectl set-timezone Europe/Berlin

## TIMEZONE ##

## SYNC VAGRANT MACHINE TIME ##

sudo apt-get install ntpdate
sudo ntpdate 1.de.pool.ntp.org

## SYNC VAGRANT MACHINE TIME ##

## HTTP SERVER / VIRTUALHOST ##

sudo a2enmod rewrite
sudo a2enmod expires
sudo a2enmod ssl
sudo a2enmod headers
sudo a2enmod http2
sudo a2enmod proxy_fcgi
sudo a2enconf php7.3-fpm
sudo a2dismod php7.3
sudo a2dismod mpm_prefork
sudo a2enmod mpm_event

sudo mkdir /etc/apache2/ssl
sudo openssl req -batch -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt

echo 'echo "memory_limit = 512M" >> /etc/php/7.3/fpm/conf.d/user.ini' | sudo -s
echo 'echo "upload_max_filesize = 100M" >> /etc/php/7.3/fpm/conf.d/user.ini' | sudo -s
echo 'echo "post_max_size = 100M" >> /etc/php/7.3/fpm/conf.d/user.ini' | sudo -s

echo 'echo "upload_max_filesize = 100M" >> /etc/php/7.3/cli/conf.d/user.ini' | sudo -s
echo 'echo "post_max_size = 100M" >> /etc/php/7.3/cli/conf.d/user.ini' | sudo -s

echo "# BUTLER
<VirtualHost *:80>
     ServerName $VM_HOSTNAME
     ServerAlias $VM_HOSTNAME
     SetEnv SHOPWARE_ENV local
     DocumentRoot $VM_SHOPWARE/public
     <Directory $VM_SHOPWARE>
         Options Indexes FollowSymLinks MultiViews
         AllowOverride All
         Order allow,deny
         Allow from all
         Require all granted
     </Directory>
</VirtualHost>

<VirtualHost *:443>
     ServerName $VM_HOSTNAME
     ServerAlias $VM_HOSTNAME
     SetEnv SHOPWARE_ENV local
     Protocols h2 http/1.1
     DocumentRoot $VM_SHOPWARE/public
     <Directory $VM_SHOPWARE>
         Options Indexes FollowSymLinks MultiViews
         AllowOverride All
         Order allow,deny
         Allow from all
         Require all granted
     </Directory>
     SSLCertificateFile /etc/apache2/ssl/apache.crt
     SSLCertificateKeyFile /etc/apache2/ssl/apache.key
     <IfModule mod_headers.c>
        Header always set Strict-Transport-Security 'max-age=15768000; includeSubDomains; preload'
     </IfModule>
</VirtualHost>
" > /etc/apache2/sites-available/000-$VM_HOSTNAME.conf

sudo a2ensite 000-$VM_HOSTNAME.conf
sudo a2dissite 000-default.conf

sudo systemctl restart apache2
sudo systemctl restart php7.3-fpm

## HTTP SERVER / VIRTUALHOST ##

## SHOPWARE JWT SECRET ##


## SHOPWARE JWT SECRET ##

##  ## SHOPWARE ##
##
##  git clone https://github.com/shopware/production.git $VM_PATH
##  cd $VM_PATH
##  composer install
##  bin/console system:install --create-database --basic-setup
##
##  ## SHOPWARE ##

##  ## MIDDLEWARE ##
##
##  cd $VM_PATH
##  composer create-project --prefer-dist laravel/laravel middleware
##
##  ## MIDDLEWARE ##

echo $DATE_WITH_TIME > $BUTLER

elsecd

echo " "
echo " |‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾|"
echo " |   APACHE / PHP / MYSQL / ELASTICSEARCH / REDIS / NODE JS / SHOWPARE 6 / MIDDLEWARE   |"
echo " |______________________________________________________________________________________|"
echo "                      ¯\_(ツ)_/¯                             ¯\_(ツ)_/¯"
echo " "

fi